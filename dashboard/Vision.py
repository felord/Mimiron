# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  dax
@Version        :  
------------------------------------
@File           :  Vision.py
@Description    :  
@CreateTime     :  2019/12/25 14:04
------------------------------------
@ModifyTime     :  
"""

from tensorboardX import SummaryWriter
import time
import torchvision

__all__ = ('ImageBoard', 'GraphBoard', 'ScalarBoard', 'LogBoard')


class ImageBoard:
    """
     预览图片集 tensor
    """

    def __init__(self):
        self.writer = SummaryWriter(time.strftime('runs/images_%Y%m%d'))

    def __call__(self, data_loader):
        # get some random training images
        data = iter(data_loader)
        images, _ = next(data)

        img_grid = torchvision.utils.make_grid(images)
        with self.writer as w:
            w.add_image('images', img_grid)


class GraphBoard:
    """
    计算图看板
    """

    def __init__(self):
        self.writer = SummaryWriter(time.strftime('runs/graphs_%Y%m%d'))

    def __call__(self, model, input_to_model):
        with self.writer as w:
            w.add_graph(model, input_to_model)


class ScalarBoard:
    """
     标量曲线看板
    """

    def __init__(self, comment):
        self.writer = SummaryWriter(time.strftime('runs/{comment}_scalars_%Y%m%d'.format(comment=comment)))

    def __call__(self, tag, val, step):
        with self.writer as w:
            w.add_scalar(tag, val, step)


class LogBoard:
    """
    log 看板
    """

    def __init__(self, comment):
        self.writer = SummaryWriter(time.strftime('runs/{comment}_logs_%Y%m%d'.format(comment=comment)))

    def __call__(self, tag, log_str, step):
        with self.writer as w:
            w.add_text(tag, log_str, step)
