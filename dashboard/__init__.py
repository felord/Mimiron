# !/usr/bin/python3
# -*- coding: utf-8 -*-
""" 看板
@Author         :  dax
@Version        :  
------------------------------------
@File           :  __init__.py.py
@Description    :  
@CreateTime     :  2019/12/25 14:03
------------------------------------
@ModifyTime     :  
"""
from .Vision import ImageBoard, GraphBoard, ScalarBoard, LogBoard
