# !/usr/bin/python3
# -*- coding: utf-8 -*-
""" restful 服务
@Author         :  dax
@Version        :  
------------------------------------
@File           :  application.py
@Description    :  
@CreateTime     :  2019/12/26 12:27
------------------------------------
@ModifyTime     :  
"""
import io
import torch
import torchvision.transforms as transforms
from flask import Flask, request, jsonify
from torch.nn import functional as Fn
from PIL import Image
from config import GlobalConfig

global_conf = GlobalConfig()


def tensor_from_location_image(path):
    """
     从本地加载图片本地测试使用
    :param path:
    :return:
    """
    with open(path, 'rb') as f:
        image_bytes = f.read()
        return transform_image(image_bytes=image_bytes)


def transform_image(image_bytes):
    """
    图片预处理
    :param image_bytes:
    :return:
    """
    trans = transforms.Compose([transforms.Resize(255),
                                transforms.CenterCrop(224),
                                transforms.ToTensor(),
                                transforms.Normalize(
                                    [0.485, 0.456, 0.406],
                                    [0.229, 0.224, 0.225])])
    image = Image.open(io.BytesIO(image_bytes))
    return trans(image).unsqueeze(0)


def prediction(image_bytes):
    """
    根据模型对图片进行断言
    :param image_bytes: 图片字节
    """
    model = torch.load(global_conf.prod_model_path)
    model.eval()
    score = model(transform_image(image_bytes))

    #     使用交叉熵计算属于各种类的概率集
    probabilities = Fn.softmax(score, dim=1)

    max_probability, index = probabilities.max(dim=1)
    return index.item(), max_probability.item()


# 服务化
app = Flask(__name__)

ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']


# Restful API
@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        image = request.files['file']
        if not image:
            return _jsonify(code=701, msg='unrecognized image')
        # 图片检测
        file_name = image.filename
        if validate_image(file_name):
            pass
        else:
            return _jsonify(code=702, msg='file type must in [ %s ]' % ','.join(ALLOWED_EXTENSIONS))
        image_bytes = image.read()
        label, probability = prediction(image_bytes)
        data = {'label': label, 'probability': round(probability * 100, 2)}
        return _jsonify(msg='successful', data=data)
    else:
        return _jsonify(code=703, msg='request method is not allowed')


# json 返回处理
def _jsonify(code=200, msg='', data=None):
    return jsonify({'code': code, 'msg': msg, 'data': data})


# 文件检测
def validate_image(filename):
    split = '.'
    return split in filename and filename.rsplit(split, 1)[1].lower() in ALLOWED_EXTENSIONS


# 入口函数
if __name__ == '__main__':
    # 运行app 指定IP 指定端口
    app.run(global_conf.host, port=global_conf.port)
