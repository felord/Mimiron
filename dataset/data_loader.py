# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  dax
@Version        :  
------------------------------------
@File           :  data_loader.py
@Description    :  
@CreateTime     :  2019/12/23 16:54
------------------------------------
@ModifyTime     :  
"""
from torchvision.datasets import ImageFolder
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from config import GlobalConfig, TrainConfig

mean = [0.464, 0.412, 0.339]
std = [0.173, 0.169, 0.163]

norm = transforms.Normalize(mean, std)
data_transforms = {
    'train': transforms.Compose([
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ColorJitter(brightness=0.5, contrast=0.5, hue=0.5),
        transforms.ToTensor(),
        norm
    ]),
    'test': transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        norm
    ]),
}


# loader = DataLoader(dataset, batch_size=2, shuffle=True)

global_conf = GlobalConfig()


class TrainDataLoader:
    def __init__(self, transform=data_transforms['train'], target_transform=None):
        dataset = ImageFolder(global_conf.train_path, transform, target_transform)
        train_conf = TrainConfig()
        self.data_loader = DataLoader(dataset, train_conf.batch_size, shuffle=True, num_workers=train_conf.num_workers)


class ValidateDataLoader:
    def __init__(self, transform=data_transforms['test'], target_transform=None):
        dataset = ImageFolder(global_conf.vali_path, transform, target_transform)
        train_conf = TrainConfig()
        self.data_loader = DataLoader(dataset, train_conf.batch_size, shuffle=False, num_workers=train_conf.num_workers)
