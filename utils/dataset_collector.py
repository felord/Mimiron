# !/usr/bin/python3
# -*- coding: utf-8 -*-
""" 数据集 下载 并使用 plt 进行grid 样本展示下载
@Author         :  dax
@Version        :
------------------------------------
@File           :  dataset_classify.py
@Description    :
@CreateTime     :  2020/1/10 9:46
------------------------------------
@ModifyTime     :
"""
import json
import urllib.request as req
from torch.utils.data import DataLoader
import torchvision
from torchvision import transforms
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
import os
import shutil
from config import GlobalConfig

means = [0.43037778, 0.38346475, 0.31376386]
stds = [0.18269944, 0.18776372, 0.19688581]


def download(json_array, target_dir, label):
    """根据json数组中的url 下载图片并保存
     # download('./b.json', './data', 'f')

    :param json_array: 格式 ['url',……]
    :param target_dir:  目标文件夹
    :param label: 标签
    """
    if not os.path.exists(target_dir):
        os.mkdir(target_dir)
    with open(json_array) as f:
        j = json.load(f)
        for i, k in tqdm(enumerate(j)):
            file_suffix = os.path.splitext(k)[-1]
            file_name = label + '.' + str(i) + file_suffix
            path = os.path.join(target_dir, file_name)

            req.urlretrieve(k, filename=path)


def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array(means)
    std = np.array(stds)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.savefig('grid.jpg')
    plt.pause(0.001)


global_config = GlobalConfig()


def classify(begin, end, is_train=True):
    """ 数据集分类取样
    :param begin: 起始数
    :param end:  结束数
    :param is_train:
    """
    target_dir = global_config.train_path if is_train else global_config.test_path
    path = _make_dir(target_dir)

    for label in global_config.labels:
        tp_path = _make_dir(os.path.join(path, label))
        images = [label + '.{}.jpg'.format(i) for i in np.arange(begin, end)]
        images_num = len(images)
        collection = images[:int((1 - global_config.vali_rate) * images_num)] if is_train else images
        for img in collection:
            src = os.path.join(global_config.data_path, img)

            dst = os.path.join(tp_path, img)
            shutil.copyfile(src, dst)

        if is_train:
            vali_path = _make_dir(global_config.vali_path)
            tp_path = _make_dir(os.path.join(vali_path, label))
            for img in images[-int(global_config.vali_rate * images_num):]:
                src = os.path.join(global_config.data_path, img)
                dst = os.path.join(tp_path, img)
                shutil.copyfile(src, dst)


def _make_dir(dataset_dir):
    """ 创建 训练集文件夹
    @param dataset_dir: dataset_dir
    """
    if not os.path.exists(dataset_dir):
        os.mkdir(dataset_dir)
    return dataset_dir


def calculating_the_mean_and_std(dataset):
    """ 计算数据集的均值和标准差用以归一化 通过计算结果及进行处理
    trans = transforms.Compose([
      transforms.Resize(256),
      transforms.CenterCrop(144),
      transforms.ToTensor(),
      ])

    dataset = torchvision.datasets.ImageFolder(path, trans)
    calculating_the_mean_and_std(dataset)
    :param dataset: 数据集 来自{ImageFolder}的 dataset  transform 需要设定
    """
    count = len(dataset)
    means = [0, 0, 0]
    stds = [0, 0, 0]
    for data, label in dataset:
        img = data
        for i in range(3):
            means[i] += img[i, :, :].mean()
            stds[i] += img[i, :, :].std()
    mean = np.asarray(means) / count
    std = np.asarray(stds) / count
    print('mean = {},std = {}'.format(mean, std))


def visual():
    trans = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(144),
        transforms.ToTensor(),
        transforms.Normalize(means, stds)
    ])

    dataset = torchvision.datasets.ImageFolder(r'C:\PycharmProjects\Mimiron\utils', trans)
    loader = DataLoader(dataset, batch_size=len(dataset), shuffle=False)
    inputs, _ = next(iter(loader))
    out = torchvision.utils.make_grid(inputs)
    # 展示
    imshow(out, title='false')


def rename_format_batch(path, label, extension):
    """
     批量重命名格式化
     rename_format_batch('./data/0', 'f', '.jpg')
    :param extension:
    :param path: 目录
    :param label: label
    """
    files = os.listdir(path)

    for index, file in tqdm(enumerate(files)):
        old_dir = os.path.join(path, file)
        if os.path.isdir(old_dir):
            pass
        new_dir = os.path.join(path, label + '.' + str(index) + extension)
        os.rename(old_dir, new_dir)


trans = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(144),
    transforms.ToTensor(),
])

dataset = torchvision.datasets.ImageFolder(r'C:\PycharmProjects\Mimiron\utils', trans)

calculating_the_mean_and_std(dataset)
