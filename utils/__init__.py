# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  dax
@Version        :  
------------------------------------
@File           :  __init__.py.py
@Description    :  
@CreateTime     :  2019/12/23 17:42
------------------------------------
@ModifyTime     :  
"""
from .dataset_classify import classify
