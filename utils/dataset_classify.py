# !/usr/bin/python3
# -*- coding: utf-8 -*-
""" 数据集分类
@Author         :  dax
@Version        :  
------------------------------------
@File           :  dataset_classify.py
@Description    :  
@CreateTime     :  2019/12/18 14:46
------------------------------------
@ModifyTime     :  
"""
import os
import shutil
from config import GlobalConfig
import numpy as np

global_config = GlobalConfig()
typ = ['f', 't']


def classify(begin, end, is_train=True):
    """ 数据集分类取样
    :param begin: 起始数
    :param end:  结束数
    :param is_train:
    """
    target_dir = global_config.train_path if is_train else global_config.test_path
    path = _make_dir(target_dir)

    for tp in typ:
        tp_path = _make_dir(os.path.join(path, tp))
        images = [tp + '.{}.jpg'.format(i) for i in np.arange(begin, end)]
        images_num = len(images)
        collection = images[:int((1 - global_config.vali_rate) * images_num)] if is_train else images
        for img in collection:
            src = os.path.join(global_config.data_path, img)

            dst = os.path.join(tp_path, img)
            shutil.copyfile(src, dst)

        if is_train:
            vali_path = _make_dir(global_config.vali_path)
            tp_path = _make_dir(os.path.join(vali_path, tp))
            for img in images[-int(global_config.vali_rate * images_num):]:
                src = os.path.join(global_config.data_path, img)
                dst = os.path.join(tp_path, img)
                shutil.copyfile(src, dst)


def _make_dir(dataset_dir):
    """ 创建 训练集文件夹
    @param dataset_dir: dataset_dir
    """
    if not os.path.exists(dataset_dir):
        os.mkdir(dataset_dir)
    return dataset_dir


classify(0, 144)
