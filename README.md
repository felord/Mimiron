## Mimiron  

基于 **Pytorch** 深度学习的N分类应用，已经实现模块化，可视化，迁移学习，多模型权重预测。


### 数据集

- 数据集命名规则为： `<label>.<index>.<extension>`， 可通过 `utils` 包下批量命名方法 `rename_format_batch` 处理
- 数据集需要自行计算归一化 `mean` 和 `std` 值 ， 可通过 `utils` 包下方法 `calculating_the_mean_and_std` 计算
- 数据集训练集验证集分类 由 `utils` 包下 `classify` 方法提供


### 训练

1. 配置训练参数，默认采用残差神经网络 `resnet50` 模型进行迁移训练
2. 使用 `tensorboard --logdir=runs` 启动 **tensorboard** 
3. 执行 `training.py`
4. 通过 `http://localhost:6006/` 可打开可视化对准确度和损失进行监控

![可视化](./visual.png)

### 启动

启动 `application.py` 开启图片二分类识别服务 

### 配置

配置参见 `application.yml`

### Restful API

### 使用方式

部署发布后 通过接口 `http://ip:port/predict` 进行 **POST** 请求，默认 `http://localhost:8000/predict`, 参考 **curl** :


```
 curl -X POST \
  http://localhost:8000/predict \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 171394' \
  -H 'Content-Type: multipart/form-data; boundary=--------------------------106661540861282323353881' \
  -H 'Host: localhost:8000' \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F 'file=@/C:/Users/a/Desktop/car/QQ截图20191209191120.png'
```


### 附·报文返回格式

```json
 {
    "code": 200,
    "data": {
        "label": 1,
        "probability": 98.13
    },
    "msg": "successful"
 }
```


|编码|含义|
|---|---|
|200| 识别成功返回label以及对应概率|
|701|无法识别| 
|702|文件类型格式不匹配| 
|703|接口请求方式不支持| 



### 附·镜像源

- https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
- https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
- https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/
- https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/menpo/
- https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda/
- https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/
- https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/