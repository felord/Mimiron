# !/usr/bin/python3
# -*- coding: utf-8 -*-
""" 迁移学习训练
@Author         :  dax
@Version        :  
------------------------------------
@File           :  training.py
@Description    :  
@CreateTime     :  2019/12/24 14:45
------------------------------------
@ModifyTime     :  
"""
from tqdm import tqdm
from config import TrainConfig
from dataset import TrainDataLoader, ValidateDataLoader
from torch.autograd import Variable
from torchnet import meter
import torch.optim.lr_scheduler as lr_scheduler
import models
import torch
import os
import time
import dashboard

train_conf = TrainConfig()

a = dashboard.LogBoard


def training():
    lr = train_conf.learning_rate
    # step1: 加载预训练模型
    model = models.resnet50(train_conf.pre_model_path)
    # step2: 冻结参数
    for par in model.parameters():
        # 梯度禁用
        par.requires_grad = False
    # 定制线性层
    model.fc = torch.nn.Linear(in_features=2048, out_features=2)
    # step 3: 定义损失函数 这里使用交叉熵
    criterion = torch.nn.CrossEntropyLoss()
    # 定义优化器 使用adam 算法
    optimizer = torch.optim.Adam(model.parameters(), lr=lr, betas=(0.9, 0.999))
    # 定义学习率更新机制
    lr_reducer = lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=2, verbose=True)
    #  step4: 加载训练集和验证集
    train_data_loader = TrainDataLoader()
    validate_data_loader = ValidateDataLoader()
    # meters
    loss_meter = meter.AverageValueMeter()
    vali_loss_meter = meter.AverageValueMeter()
    vali_accuracy_meter = meter.AverageValueMeter()
    # 模型看板
    graph_board = dashboard.GraphBoard()
    train_loss_board = dashboard.ScalarBoard('train_loss')
    train_acc_board = dashboard.ScalarBoard('train_acc')
    vali_loss_board = dashboard.ScalarBoard('vali_loss')
    vali_acc_board = dashboard.ScalarBoard('vali_acc')
    log_board = dashboard.LogBoard('log')

    # 定义混淆矩阵用于评分评判
    confusion_matrix = meter.ConfusionMeter(2)

    # step5: 训练
    for epoch in range(train_conf.epochs):
        loss_meter.reset()
        confusion_matrix.reset()

        vali_loss_meter.reset()
        vali_accuracy_meter.reset()
        for i, (data, label) in tqdm(enumerate(train_data_loader.data_loader),
                                     desc='training @ epoch {}'.format(epoch + 1),
                                     total=len(train_data_loader.data_loader)):
            graph_board(model, data)
            # 训练模式
            model.train()
            inputs = Variable(data)
            # 梯度归零
            optimizer.zero_grad()
            score = model(inputs)
            loss = criterion(score, label.type(torch.LongTensor))
            loss.backward()
            optimizer.step()
            loss_meter.add(loss.item())
            confusion_matrix.add(score.data, label.type(torch.LongTensor))
            vali_accuracy, vali_loss = validate(model, validate_data_loader.data_loader)
            vali_loss_meter.add(vali_loss)
            vali_accuracy_meter.add(vali_accuracy)

        # 动态更新lr
        lr_reducer.step(loss_meter.mean, epoch)
        # cross vali
        train_cm_val = confusion_matrix.value()
        train_accuracy = 100. * (train_cm_val[0][0] + train_cm_val[1][1]) / train_cm_val.sum()

        log_board('log', 'train_loss:{},train_acc: {},vali_loss:{},vali_acc:{}'
                  .format(loss_meter.mean, train_accuracy, vali_loss_meter.mean, vali_accuracy_meter.mean),
                  epoch + 1)

        train_loss_board('Loss', float(loss_meter.mean), epoch + 1)
        train_acc_board('Accuracy', float(train_accuracy), epoch + 1)
        vali_loss_board('Loss', float(vali_loss_meter.mean), epoch + 1)
        vali_acc_board('Accuracy', float(vali_accuracy_meter.mean), epoch + 1)
        # step6: 保存模型
        if not os.path.exists(train_conf.model_dir):
            os.mkdir(train_conf.model_dir)
        model_path = time.strftime(train_conf.model_dir + '/%Y%m%d.pth')
        torch.save(model, model_path)


def validate(model, data_loader):
    # 验证模式
    model.eval()
    loss_meter = meter.AverageValueMeter()
    confusion_matrix = meter.ConfusionMeter(2)
    for i, (inputs, label) in tqdm(enumerate(data_loader), desc='validating'):
        val_input = Variable(inputs)
        score = model(val_input)
        criterion = torch.nn.CrossEntropyLoss()
        loss_meter.add(criterion(score, label.type(torch.LongTensor)).item())
        confusion_matrix.add(score.data.squeeze(), label.type(torch.LongTensor))
    cm_value = confusion_matrix.value()
    accuracy = 100. * (cm_value[0][0] + cm_value[1][1]) / cm_value.sum()
    return accuracy, loss_meter.mean


# os.system('tensorboard --logdir=runs')
training()
