# !/usr/bin/python3
# -*- coding: utf-8 -*-
""" 日志封装
@Author         :  dax
@Version        :  
------------------------------------
@File           :  log.py
@Description    :  
@CreateTime     :  2019/12/16 13:42
------------------------------------
@ModifyTime     :  
"""
import logging
import os
import sys
import time

from config import LogConfig


def log(name=None):
    """日志组件
    :param name:
    :return:
    """
    # 获取logger实例，如果参数为空则返回root logger
    log = logging.getLogger(name)
    # 创建日志输出格式
    formatter = logging.Formatter('%(asctime)s  %(name)s   %(levelname)s   %(message)s')

    # 指定输出的文件路径
    log_conf = LogConfig()
    if not os.path.exists(log_conf.log_dir):
        os.mkdir(log_conf.log_dir)
    path_formatter = os.path.join(log_conf.log_dir, log.name + '_' + '%Y%m%d.log')
    path = time.strftime(path_formatter)
    file_handler = logging.FileHandler(path)
    # 设置文件处理器，加载处理器格式
    file_handler.setFormatter(formatter)

    # 控制台日志
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.formatter = formatter

    # 为logger添加的日志处理器
    log.addHandler(file_handler)
    log.addHandler(console_handler)

    # 指定日志的最低输出级别，默认为warn级别
    log.setLevel(log_conf.level)
    return log
