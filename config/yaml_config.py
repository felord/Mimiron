# !/usr/bin/python3
# -*- coding: utf-8 -*-
""" 配置
@Author         :  dax
@Version        :  
------------------------------------
@File           :  yaml_config.py
@Description    :  
@CreateTime     :  2019/12/23 15:29
------------------------------------
@ModifyTime     :  
"""
import yaml
import os

__all__ = ('TrainConfig', 'GlobalConfig', 'LogConfig')


def _yaml_reader(path=None):
    """ read yaml config
    :param path
    :return: config
    """
    yml = open(path, encoding='utf-8')
    config = yaml.safe_load(yml)
    if config:
        return config
    else:
        raise RuntimeError('no config find in path: {}'.format(path))


class BaseConfig:
    def __init__(self, config_file='application.yml'):
        cur_path = os.path.split(os.path.realpath(__file__))[0][:]
        self.root_path = cur_path[:cur_path.find('config')]
        self.config = _yaml_reader(os.path.join(self.root_path, config_file))


class TrainConfig(BaseConfig):
    def __init__(self):
        super().__init__()
        self.config = self.config['train']
        self.num_workers = self.config['num-workers']
        self.batch_size = self.config['batch-size']
        self.learning_rate = self.config['learning-rate']
        self.epochs = self.config['epochs']
        self.model_dir = os.path.join(self.root_path, self.config['model-dir'])
        self.pre_model_path = self.config['pre-model-path']


class LogConfig(BaseConfig):
    def __init__(self):
        super().__init__()
        self.config = self.config['logging']
        self.log_dir = os.path.join(self.root_path, self.config['dir'])

        level_values = {'CRITICAL': 50, 'FATAL': 50, 'ERROR': 40, 'WARNING': 30, 'WARN': 30, 'INFO': 20, 'DEBUG': 10,
                        'NOTSET': 0}
        level_key = self.config['level'].upper()
        self.level = level_values[level_key]


class GlobalConfig(BaseConfig):

    def __init__(self):
        super().__init__()
        self.config = self.config['global']
        dirs = self.config['dirs']
        self.vali_rate = self.config['vali-rate']
        self.host = self.config['host']
        self.port = self.config['port']
        self.labels = self.config['labels']
        self.prod_model_path = self.config['prod-model-path']
        self.data_path = os.path.join(self.root_path, dirs['data'])
        self.test_path = os.path.join(self.root_path, dirs['test'])
        self.train_path = os.path.join(self.root_path, dirs['train'])
        self.vali_path = os.path.join(self.root_path, dirs['vali'])
